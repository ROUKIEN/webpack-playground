'use strict'

import hello from './components/hello'
import form from './components/form'

document.body.appendChild(form('name'))

document.body.appendChild(hello('Buddy'))
document.body.appendChild(hello('McFly'))
document.body.appendChild(hello('Lannister'))
document.body.appendChild(hello('Pink dragonzzz'))
