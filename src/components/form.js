'use strict'

export default function form (label) {
  var element = document.createElement('input')
  element.placeholder = `some ${label}`

  return element
}
