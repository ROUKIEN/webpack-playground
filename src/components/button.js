'use strict'

export default function button (label) {
  var button = document.createElement('button')
  button.innerHTML = label

  return button
}
