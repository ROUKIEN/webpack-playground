'use strict'

export default function component (name) {
  var element = document.createElement('div')
  element.innerHTML = `Hello ${name}`

  return element
}
