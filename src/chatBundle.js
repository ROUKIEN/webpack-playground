'use strict'

import form from './components/form'
import button from './components/button'

document.body.appendChild(form('type a message...'))
document.body.appendChild(button('Send !'))
